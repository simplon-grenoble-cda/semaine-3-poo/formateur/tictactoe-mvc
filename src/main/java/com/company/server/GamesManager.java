package com.company.server;


import com.company.game.Game;

import java.util.HashMap;
import java.util.Map;

/**
 * The GamesManager class manages all the instances of Game on the server
 */
public class GamesManager {

    private int gameIdCounter = 1;
    private Map<Integer, Game> gamesMap = new HashMap<>();
    private static GamesManager singletonInstance;

    public static GamesManager getGamesManagerInstance() {
        if (singletonInstance == null) {
            singletonInstance = new GamesManager();
        }
        return singletonInstance;
    }

    private GamesManager() {
    }

    public Game createNewGame() {
        int gameId = gameIdCounter;
        gameIdCounter++;
        Game newGame = new Game(gameId);
        this.gamesMap.put(gameId, newGame);
        return newGame;
    }

    /**
     * Returns the game instance mapped with the provided gameId or null if there is no corresponding
     * instance.
     * @param gameId
     * @return
     */
    public Game getGame(int gameId) {
        return this.gamesMap.get(gameId);
    }

}
