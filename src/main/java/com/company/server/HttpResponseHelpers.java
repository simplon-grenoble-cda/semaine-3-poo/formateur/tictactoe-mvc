package com.company.server;

import com.sun.net.httpserver.HttpExchange;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;

public class HttpResponseHelpers {


    /**
     * Helper method to reply with a JSON error message to a HTTP request. The JSON
     * is formatted according to the TicTacToe API specification (see https://gitlab.com/simplon-grenoble-cda/semaine-1-poo/formateur/poo-exercices/exhttptictactoe)
     * Send a response with status code 400.
     *
     * @param httpExchange The exchange object, from the {@link com.sun.net.httpserver.HttpHandler} calling
     *                     this method
     * @param errorString The string to put in the "error" property of the JSON response message
     * @param message The string to put in the "message" property of the JSON response message
     */
    public static void sendResponseError(
            HttpExchange httpExchange,
            String errorString,
            String message
    ) throws IOException {
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("error", errorString);
        jsonResponse.put("message", message);
        sendJSONResponse(httpExchange, 400, jsonResponse);
    }

    /**
     * Helper method to send a HTTP response, with the provided status code,
     * and the provided JSON Object as response body. The headers for JSON content-type
     * are added to the response.
     *
     * @param httpExchange The {@link HttpExchange} object from the handler using this helper
     * @param httpStatusCode The HTTP status code to use for the response
     * @param jsonResponse The JSON object to send as response body
     * @throws IOException
     */
    public static void sendJSONResponse(
            HttpExchange httpExchange,
            int httpStatusCode,
            JSONObject jsonResponse) throws IOException {
        sendJSONResponse(httpExchange, httpStatusCode, jsonResponse.toString());
    }

    /*
     * Helper method to send a HTTP response, with the provided status code,
     * and the provided JSON Object as response body. The headers for JSON content-type
     * are added to the response.
     *
     * @param httpExchange The {@link HttpExchange} object from the handler using this helper
     * @param httpStatusCode The HTTP status code to use for the response
     * @param jsonResponse The JSON string to send as response
     * @throws IOException
     */
    public static void sendJSONResponse(
            HttpExchange httpExchange,
            int httpStatusCode,
            String responseBody
    ) throws IOException {
        httpExchange.getResponseHeaders().add("Content-type", "application/json");
        httpExchange.sendResponseHeaders(httpStatusCode, responseBody.getBytes().length);
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(responseBody.getBytes());
        outputStream.close();
    }

}
