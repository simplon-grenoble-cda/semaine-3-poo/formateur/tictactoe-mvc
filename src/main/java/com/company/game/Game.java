package com.company.game;

/**
 * A Game represent a Tic Tac Toe game being played. It has a grid,
 * 2 players, and other informations to know the game current status.
 */
public class Game {

    int id;
    private Player playerA;
    private Player playerB;
    private TicTacToeGrid grid = new TicTacToeGrid();
    private PlayerToken nextRoundPlayer = PlayerToken.A;

    public Game(int id) {
        this.id = id;
        this.playerA = new Player("Player A");
        this.playerB = new Player("Player B");
    }

    public Game(int id, Player playerA, Player playerB) {
        this.id = id;
        this.playerA = playerA;
        this.playerB = playerB;
    }

    public void playToken(int cellIdx, PlayerToken player) throws BadPlayerException, CellNotEmptyException {
        if (player != this.nextRoundPlayer) {
            throw new BadPlayerException();
        }
        this.grid.addToken(cellIdx, player);
        this.switchNextRoundPlayer();
    }

    /**
     * Switch this.nextRoundPlayer from player A to player B or vice versa
     */
    private void switchNextRoundPlayer() {
        if (this.nextRoundPlayer == PlayerToken.A) {
            this.nextRoundPlayer = PlayerToken.B;
        } else {
            this.nextRoundPlayer = PlayerToken.A;
        }
    }


    public PlayerToken[] getGridCells() {
        return this.grid.getGrid();
    }

    public PlayerToken getNextRoundPlayer() {
        return nextRoundPlayer;
    }

    /**
     * Returns the winner Player if there is any.
     * Returns null otherwise
     */
    public PlayerToken getWinner() {
        return this.grid.getWinner();
    }

    public Player getPlayer(PlayerToken playerToken) {
        if (playerToken == PlayerToken.A) {
            return this.playerA;
        } else if (playerToken == PlayerToken.B) {
            return this.playerB;
        } else {
            return null;
        }
    }

    public void setPlayer(PlayerToken playerToken, Player player) {
        if (playerToken == PlayerToken.A) {
            this.playerA = player;
        } else if (playerToken == PlayerToken.B) {
            this.playerB = player;
        }
    }

    public int getId() {
        return this.id;
    }

}
